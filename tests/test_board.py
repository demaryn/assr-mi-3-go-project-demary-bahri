from gomi3.board import *

def test_load():
    b = Board(3)

    state = '''
    ---
    -w-
    b--
    '''

    b.load(state)

    for y in range(3):
        for x in range(3):
            if ((x,y) == (0,2)):
                assert b.color_at(x,y) == Color.BLACK, "(x=0,y=2) should be black"
            elif ((x,y) == (1,1)):
                assert b.color_at(x,y) == Color.WHITE, "(x=1,y=1) should be white"
            else:
                assert b.color_at(x,y) == Color.EMPTY

def test_load2():
    print(Board(3).get_tableau())
    board = ['--b', '-bw', 'b--']
    for line in board:
        for char in line:
            print(board.index(line), line.index(char))

def test_tableau():
    tab = Board(3)

    tab.set_color(Color(1), 1, 2)

    if tab.color_at(1, 2) != Color(1):
        raise ValueError('Board.set_color() is not correctly implemented')

    if tab.get_tableau() is None:
        raise ValueError('Board.get_tableau() is not correctly implemented')

    if tab.clear() is not None:
        raise ValueError('Board.clear() is not correctly implemented')
    
if __name__ == "__main__":
    test_tableau()
