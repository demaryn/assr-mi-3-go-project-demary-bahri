'''The board module.

This module is in charge of the very basic handling of the go board.
'''
from array import array
from collections import namedtuple
from enum import Enum


class Color(Enum):
    '''The color of a go board intersection.'''
    EMPTY = 0
    BLACK = 1
    WHITE = 2


Intersection = namedtuple('Intersection', ['x', 'y'])


class Board:
    '''The main class to manage a go board.'''
    def __init__(self, size: int) -> None:
        '''Initialize an empty board of size*size intersections.'''
        self.tableau = [[Color(0) for x in range(size)] for y in range(size)]
        self.size = size

    def get_tableau(self) -> array:
        return self.tableau

    def clear(self) -> None:
        '''Clear all intersections such that they all become empty.'''
        for x in range(self.size):
            for y in range(self.size):
                self.set_color(Color(0), x, y)

    def load(self, board_state: str) -> None:
        '''Sets all intersection colors from board_state.

        board_state is a string that contains size*size characters (excluding spacing characters).
                    each character represents the color of an intersection ('-', 'b' or 'w').
        '''
        board = board_state.strip().replace(" ", "").splitlines()
        if len(board) == self.size:
            for line in board:
                for char in line:
                    if char == "-":
                        self.set_color(Color.EMPTY, board.index(line), line.index(char))
                    if char == "b":
                        self.set_color(Color.BLACK, board.index(line), line.index(char))
                    if char == "w":
                        self.set_color(Color.WHITE, board.index(line), line.index(char))
        else: 
            raise SyntaxError("Input format not valid")

    def set_color(self, color: Color, x: int, y: int) -> None:
        '''Set the color of the (x,y) intersection of the board.'''
        self.tableau[x][y] = color

    def color_at(self, x: int, y: int) -> Color:
        '''Get the color of the (x,y) intersection of the board.'''

        current_color = self.tableau[x][y]
        if current_color == Color(0):
            return Color(0)
        if current_color == Color(1):
            return Color(1)
        if current_color == Color(2):
            return Color(2)

    def stone_group_at(self, x: int, y: int) -> [Intersection]:
        '''Get the maximum-sized stone group that starts from the (x,y) intersection of the board.'''
        raise NotImplementedError
